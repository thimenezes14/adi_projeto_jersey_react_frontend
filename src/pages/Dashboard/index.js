import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Navbar from 'react-bootstrap/Navbar';

import ExcluirUsuario from '../../components/ExcluirUsuario';
import FormDados from '../../components/FormDados';

import { withRouter } from 'react-router-dom';
import { FaChalkboardTeacher, FaSignOutAlt } from 'react-icons/fa';

import { logout, getToken } from '../../services/auth';
import api from '../../services/api';

import './dashboard.css';

function Dashboard(props) {

  const [user, setUser] = useState({});

  const sair = () => {
    logout();
    props.history.push('/login');
  }

  useEffect(() => {

    const loadUser = async () => {
      const userData = (await api.get('/usuario')).data;
      const loggedUser = userData.filter(u => u.email === getToken())
      return loggedUser;
    }

    loadUser()
      .then(userData => {
        if (userData.length > 0)
          setUser(userData[0]);
        else {
          logout();
          props.history.push('/login');
        }
      })
      .catch(err => {
        console.log(err);
        logout();
        props.history.push('/login');
      })
  }, [props])

  return (
    <div>
      <h1><FaChalkboardTeacher /> Dashboard</h1>
      
      <Navbar variant="dark" className="justify-content-center">
        <Navbar.Brand>{user.nome}</Navbar.Brand>
        <Button variant="danger" onClick={sair}><FaSignOutAlt /> SAIR</Button>
      </Navbar>
    
      <Tabs className=" w-100 justify-content-center tabs" defaultActiveKey="user-data" id="uncontrolled-tab-example">
        <Tab eventKey="user-data" title="Meus Dados">
          <FormDados user={user} action="update" history={props.history}/>
        </Tab>
        <Tab eventKey="remove-profile" title="Excluir Perfil" style={{color: 'white'}}>
          <ExcluirUsuario user={user} history={props.history}/> 
        </Tab>
      </Tabs>
    </div>
  );
}

export default withRouter(Dashboard);