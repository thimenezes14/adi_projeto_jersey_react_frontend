import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { withRouter } from 'react-router-dom';
import { FaUserLock, FaSignInAlt, FaExclamationCircle } from 'react-icons/fa';

import api from '../../services/api';
import {login} from '../../services/auth';

function Login(props) {
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [loading, setLoading] = useState(false);
  const [erro, setErro] = useState({status: false, mensagem: ''});

  const checkLogin = async (email, senha) => {
    setLoading(true);
    try{
      const user = (await api.post('/autenticacao', {
        email,
        senha
      })).data;
      return user;
    }catch(err) {
      console.log(err);
      return false;
    }finally {
      setLoading(false);
    }
  }

  const handleSubmit = async e => {
    e.preventDefault();
    
    await checkLogin(email, senha)
      .then(auth => {
        if(auth) {
          login(email);
          setErro({status: false, mensagem: ''})
          props.history.push('/dashboard');
        } else {
          setErro({status: true, mensagem: 'E-mail e/ou senha incorretos. '})
        }
      })
      .catch(err => {
        setErro({status: true, mensagem: `Erro ao verificar credenciais. ${err}`})
      });
  }

  return (
    <div>
      <h1><FaUserLock /> Login</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group >
            <Form.Control type="email" placeholder="E-mail" value={email} onChange={e => setEmail(e.target.value)} />
        </Form.Group>

        <Form.Group >
            <Form.Control type="password" placeholder="Senha" value={senha} onChange={e => setSenha(e.target.value)}/>
        </Form.Group>

        <Form.Group >
            <Button type="submit" variant="info"><strong><FaSignInAlt /> ENTRAR</strong></Button>
        </Form.Group>
      </Form>
      {loading && (
        <p>Autenticando...</p>
      )}
      {erro.status && (
        <div className="bg-danger text-white" style={{margin: '5px auto'}}>
          <p><FaExclamationCircle /> {erro.mensagem}</p>
        </div>
      )}
    </div>
  );
}

export default withRouter(Login);