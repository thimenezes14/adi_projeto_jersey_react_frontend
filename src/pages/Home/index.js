import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';

import { withRouter } from 'react-router-dom';
import { FaHome, FaEdit } from 'react-icons/fa';

import Usuarios from '../../components/Usuarios';
import ModalNovoUsuario from '../../components/ModalNovoUsuario';

import api from '../../services/api';

function Home(props) {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [modalShow, setModalShow] = useState(false);

  async function loadUsers() {
    const res = (await api.get('/usuario')).data;
    return res;
  }
  
  useEffect(()=> {
    loadUsers()
      .then(users => {setUsers(users); setLoading(false)})
      .catch(err => console.log(err));
  }, [])

  return (
    <div>
      <h1><FaHome /> Home</h1>

      {loading ? 'Carregando...' : <Usuarios users={users} />}
      
      <ModalNovoUsuario show={modalShow} onHide={() => setModalShow(false)} history={props.history} />
      
      <Button variant="success" size="lg" block onClick={()=> setModalShow(true)}>
        <FaEdit /> NOVO USUÁRIO
      </Button>
      <br />
    </div>
  );
}

export default withRouter(Home);