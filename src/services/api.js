import axios from 'axios';
//import { getToken } from "./auth";

const MEU_ENDERECO = process.env.REACT_APP_API_BACKEND;

const api = axios.create({
    baseURL: MEU_ENDERECO
});

/*api.interceptors.request.use(async config => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});*/

export default api;