import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Menu from './components/Menu';
import Home from './pages/Home';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';

import { isAuthenticated } from './services/auth';

import './App.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
        )
    }
  />
);

const CheckRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Redirect to={{ pathname: "/dashboard", state: { from: props.location } }} />

      ) : (
          <Component {...props} />
        )
    }
  />
);


export default function Routes() {
  return (
    <BrowserRouter>
      <Menu />
      <div className="content">
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/home" />} />
          <CheckRoute exact path="/home" component={Home} />
          <CheckRoute exact path="/login" component={Login} />
          <PrivateRoute exact path="/dashboard" component={Dashboard} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}