import React, { useEffect, useState, useCallback } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { FaEdit } from 'react-icons/fa';

import api from '../../services/api';
import { logout } from '../../services/auth';

export default function FormDados(props) {
    const [user, setUser] = useState({});
    
    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');

    const [checkNome, setCheckNome] = useState({});
    const [checkEmail, setCheckEmail] = useState({});
    const [checkSenha, setCheckSenha] = useState({});

    useEffect(()=> {
        if(props.user) {
            setUser(props.user);
            setCheckNome({error: false});
            setCheckEmail({error: false});
            setCheckSenha({error: false});
        }
    }, [props])

    useCallback(()=> {
        return ((checkNome.error === false) && (checkEmail.error === false) && (checkSenha.error === false))
    }, [checkNome, checkEmail, checkSenha]);

    const handleNome = e => {
        const value = e.target.value;
        setNome(value);
        user.nome = e.target.value;
        
        if(RegExp(/[\wA-ZÀ-ú]/).test(value.trim())) {
            setCheckNome({error: false});
        } else {
            setCheckNome({error: true, message: 'Preencha um nome válido. '});
        }
    }
    const handleEmail = e => {
        const value = e.target.value;
        setEmail(value);
        user.email = e.target.value;
        
        if(RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/).test(value.trim())) {
            setCheckEmail({error: false});
        } else {
            setCheckEmail({error: true, message: 'Preencha um e-mail válido. '});
        }
    }
    const handleSenha = e => {
        const value = e.target.value;
        setSenha(value);
        user.senha = e.target.value;
        
        if(RegExp(/[\wA-ZÀ-ú0-9]{3,}/).test(value.trim())) {
            setCheckSenha({error: false});
        } else {
            setCheckSenha({error: true, message: 'Preencha uma senha válida (min.: 3 caracteres). '});
        }
    }

    const validateData = () => {
        return ((checkNome.error === false) && (checkEmail.error === false) && (checkSenha.error === false));    
    }
    
    const handleSubmit = async e => {
        e.preventDefault();

        if(!validateData())
            return false;

        if(props.action === 'update') {
            await api.put(`/usuario/${user.id}`, user)
                .then(()=> {
                    logout();
                    props.history.push('/home');
                })
                .catch(err => console.log(err));
        } else if(props.action === 'create') {
            await api.post(`/usuario`, user)
                .then(()=> {
                    props.history.push('/login');
                })
                .catch(err => console.log(err));
        } else {
            console.error("Erro: action não definida. ");
        }
    }

    return (
        <div>
            <br />
            <Form onSubmit={handleSubmit}>
                <Form.Group>
                    <Form.Label>Nome</Form.Label>
                    <Form.Control type="text" placeholder="Nome" defaultValue={user.nome} value={user.nome || nome} onChange={handleNome} onBlur={validateData}/>
                    {checkNome.error && 
                        (<div className="bg-danger text-white">
                            {checkNome.message}
                        </div>)
                    }
                </Form.Group>

                <Form.Group>
                    <Form.Label>E-mail</Form.Label>
                    <Form.Control type="email" placeholder="E-mail" defaultValue={user.email} value={user.email || email} onChange={handleEmail} onBlur={validateData}/>
                    {checkEmail.error && 
                        (<div className="bg-danger text-white">
                            {checkEmail.message}
                        </div>)
                    }
                </Form.Group>

                <Form.Group>
                    <Form.Label>Senha</Form.Label>
                    <Form.Control type="password" placeholder="Senha" defaultValue={user.senha} value={user.senha || senha} onChange={handleSenha} onBlur={validateData}/>
                    {checkSenha.error && 
                        (<div className="bg-danger text-white">
                            {checkSenha.message}
                        </div>)
                    }
                </Form.Group>

                <Button variant="primary" type="submit">
                    <strong><FaEdit /> SALVAR </strong>
                </Button>
                
            </Form>

            <br />
        </div>
    );
}