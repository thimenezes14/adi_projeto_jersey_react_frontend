import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

import { FaEraser } from 'react-icons/fa';

import api from '../../services/api';
import { logout } from '../../services/auth';

export default function ExcluirUsuario(props) {
    
    const excluir = async () => {
        await api.delete(`usuario/${props.user.id}`)
            .then(()=> {
                logout();
                props.history.push('/home');
            })
            .catch(err => console.log(err));
    }

    return (
        <div>
            <Alert variant="danger">
                <h1>ATENÇÃO!</h1>
                <p>
                    Ao clicar em "EXCLUIR USUÁRIO", não será possível recuperar seus dados.
                </p>
                <p>
                    <Button variant="danger" onClick={excluir}><FaEraser /> EXCLUIR USUÁRIO</Button>
                </p>
            </Alert>
        </div>
    );
}