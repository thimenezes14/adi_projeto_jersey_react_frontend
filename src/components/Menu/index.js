import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {FaDesktop} from 'react-icons/fa';
import {withRouter} from 'react-router-dom';
import './index.css';

function Menu(props) {
    return (
        <>
            <Navbar expand="lg" variant="dark" className="menu">
                <Navbar.Brand><FaDesktop /> Projeto - ADI</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link onClick={() => props.history.push('/home')}>Home</Nav.Link>
                        <Nav.Link onClick={() => props.history.push('/login')}>Login</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default withRouter(Menu);