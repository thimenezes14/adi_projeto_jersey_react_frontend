import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';

export default function Usuarios(props) {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        setUsers(props.users);
    }, [props])

    return (
        <div>
            <h2>Usuários</h2>
            <Table className="text-white" responsive>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NOME</th>
                        <th>E-MAIL</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        users.map(user => (
                            <tr key={user.id}>
                                <td><p>{user.id}</p></td>
                                <td><p>{user.nome}</p></td>
                                <td><p>{user.email}</p></td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </div>
    );
}