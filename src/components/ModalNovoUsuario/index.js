import React from 'react';
import Modal from 'react-bootstrap/Modal';

import FormDados from '../FormDados';

export default function ModalNovoUsuario(props) {
    return (
        <div>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                className="text-center"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Novo Usuário
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FormDados action="create" history={props.history} />
                </Modal.Body>
            </Modal>
        </div>
    );
}